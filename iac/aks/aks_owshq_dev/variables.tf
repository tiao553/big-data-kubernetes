variable "client_id" {
    default = ""
}

variable "client_secret" {
    default = ""
}

variable "agent_count" {
    default = 7
}

variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
}

variable "dns_prefix" {
    default = "dns"
}

variable cluster_name {
    default = "aks-owshq-dev"
}

variable resource_group_name {
    default = "k8s-aks-owshq-dev"
}

variable location {
    default = "East US 2"
}
